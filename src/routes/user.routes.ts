import { Request, Router } from 'express';
import passport from 'passport';

import UserController from '../controllers/user.controller';
import { checkLoggedin } from '../middlewares/authuser';

const userRoutes = Router();

userRoutes.post('/login', 
    passport.authenticate('local', { successMessage: 'Login successful', failureMessage: 'Incorrect data'}), 
    (req, res) => { res.send({'message': 'Login successful'}) }
);

userRoutes.post('/logout', (req, res, next) => {
    req.logout(err => {
        if(err) { return next(err); }});
        res.send({'message': 'Logout successful'});
});


userRoutes.get('/account/:id', checkLoggedin, UserController.getUserById);
userRoutes.post('/register', UserController.createUser);
userRoutes.put('/update/:id', checkLoggedin,UserController.updateUser);
userRoutes.delete('/delete/:id', checkLoggedin, UserController.deleteUser);

export default userRoutes;