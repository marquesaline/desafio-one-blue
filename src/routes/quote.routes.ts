import { Router } from 'express';

import QuoteController from '../controllers/quote.controller';
import { checkLoggedin } from '../middlewares/authuser';

const quoteRoutes = Router();

quoteRoutes.get('/:username/quotes', QuoteController.getQuotesByUser);
quoteRoutes.get('/:username/quotes/:id', QuoteController.getQuoteById);
quoteRoutes.post('/:username/quotes/add', checkLoggedin, QuoteController.addQuote);
quoteRoutes.put('/:username/quotes/update/:id', checkLoggedin, QuoteController.updateQuote);
quoteRoutes.delete('/:username/quotes/delete/:id', checkLoggedin, QuoteController.deleteQuote);

quoteRoutes.get('/quotes', QuoteController.getAllQuotes);

export default quoteRoutes;