import { Optional } from 'sequelize';

export interface IQuote {
    id: number;
    id_user: number;
    text: string;
    author: string;
    created: string;
}

export interface QuoteInput extends Optional<IQuote, 'id' | 'id_user' | 'created'> {};