import { UserModel } from '../database/models/user.model';
import { UserInput } from '../interfaces/user.interface';

class UserService {

    public async create(user: UserInput) {

        const checkData = await this.checkEmailAndUsername(user.email, user.username);
        
        if(checkData?.find) {
            return {
                error: checkData.result
            };
        }

        try {
            const userCreated = await UserModel.create(user);
            return {
                id: userCreated.id,
                username: userCreated.username,
                email: userCreated.email
            }

        } catch(error) {

            console.log(error);
            return 'Error creating user';
        }
    }

    public async update(id: number, userToUpdate: UserInput) {
        
        try {
            const user = await UserModel.findOne({ where: { id } });

            if(user) {
                const userUpdated = await user.update(userToUpdate);
                return {
                    id: userUpdated.id,
                    username: userUpdated.username,
                    email: userUpdated.email
                }
            }

            return 'User not found';
            
        } catch(error) {
            console.log(error);
            return 'Error updating user';
        }
    }

    public async getById(id: number) {
        try {
            const user = await UserModel.findOne({ where: { id } });

            if(user) {
                return {
                    id: user.id,
                    username: user.username,
                    email: user.email,
                    password: user.password
                }
            }

            return 'User not found';

        } catch(error) {
            console.log(error);
            return 'Error trying to find user';
        }
    }

    public async getUserByUsername(username: string) {
        
        const user = await UserModel.findOne({ where: { username } });

        if(user) {
            return {
                id: user.id,
                username: user.username,
                email: user.email,
                password: user.password,
            }
        }

        return 'User not found';
    }

    public async delete(id: number) {
        try {
            const user = await UserModel.destroy({ where: { id } });

            if(user == 0) {
                return 'User not found';
            }

            return true;

        } catch(error) {
            console.log(error);
            return 'Error trying to find user';
        }
    }

    public async checkEmailAndUsername(email: string, username: string) {
        try {
            const userEmail = await UserModel.findOne({ where: { email } });
            if(userEmail) {
                return {
                    find: true,
                    result: 'Email already registered'
                }
            };

            const userName = await UserModel.findOne({ where: { username } });
            if(userName) {
                return {
                    find: true,
                    result: 'Username already registered'
                }
            };

            return {find: false};

        } catch(error) {
            console.log(error);
        }
    }

    public async checkPermissions(id?: number, username?: string) {

        let user;

        if(id) {
            user = await UserModel.findOne({ where: { id }});
        } 

        if(username) {
            user = await UserModel.findOne({ where: { username: username }});
        }
       
        if(user?.admin) { return true }
        else { return false }
    }

}

export default new UserService();