import { QuoteModel } from '../database/models/quotes.model';
import { QuoteInput } from '../interfaces/quote.interface';
import UserService from './user.service';

class QuoteService {
    
    public async add(quote: QuoteInput, username: string) {

        const user = await UserService.getUserByUsername(username);

        if(typeof(user) != 'string') {
           
            quote.id_user = user.id;
            try {
                
                const quoteCreated = await QuoteModel.create(quote);
                return {
                    id: quoteCreated.id,
                    author: quoteCreated.author,
                    text: quoteCreated.text,
                    created: quoteCreated.created
                }
            
            } catch(error) {
                console.log(error);
                return 'Error creating user';
            }

        } else {
            return user;
        }
    }

    public async update(id: number, quoteToUpdate: QuoteInput) {
        try {
            const quote = await QuoteModel.findOne({ where: { id: id }});

            if(quote) {
                const quoteUpdated = await quote.update(quoteToUpdate);
                return {
                    id: quoteUpdated.id,
                    author: quoteUpdated.author,
                    text: quoteUpdated.text,
                    created: quoteUpdated.created
                }
            }
            return 'Quote not found';
        
        } catch(error) {
            console.log(error);
            return 'Error creating user';
        }

    }

    public async getAllByUsername(username: string) {

        const user = await UserService.getUserByUsername(username);

        if(typeof(user) != 'string') {
            try {
                const quotes = await QuoteModel.findAll({ where: { id_user: user.id }, order: [['createdAt', 'ASC']]});

                let quotesToShow: any = [];

                quotes.forEach(quote => {
                    quotesToShow.push({
                        id: quote.id,
                        author: quote.author,
                        text: quote.text,
                        created: quote.created
                    });
                });
                return quotesToShow;
            
            } catch(error) {
                console.log(error);
                return 'Error searching quotes';
            }

        } else {
            return user;
        }
    }

    public async getQuotes() {
        try {
            const quotes = await QuoteModel.findAll({ order: [['createdAt', 'ASC']]});

            let quotesToShow: any = [];

            for(let i = 0; i < quotes.length; i++) {
                let user = await UserService.getById(quotes[i].id_user);
                
                await quotesToShow.push({
                    id: quotes[i].id,
                    user: typeof(user) != 'string' ? user.username: 'User not found',
                    author: quotes[i].author,
                    text: quotes[i].text,
                    created: quotes[i].created
                });
            }
            return quotesToShow;
        
        } catch(error) {
            console.log(error);
            return 'Error searching quotes';
        }
    }

    public async getById(id: number) {
        try {
            const quote = await QuoteModel.findOne({ where: { id: id } });

            if(quote) {
                return {
                    id: quote.id,
                    author: quote.author,
                    text: quote.text,
                    created: quote.created
                }
            }
            return 'Quote not found';

        } catch(error) {
            console.log(error);
            return 'Error trying to find quote';
        }
    }

    public async delete(id: number) {
        try {
            const quote = await QuoteModel.destroy({ where: { id: id } });

            if(quote == 0) {
                return 'Quote not found';
            }
            return true;

        } catch(error) {
            console.log(error);
            return 'Error trying to find quote';
        }
    }
}
export default new QuoteService();