import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import passport from 'passport';

import { UserInput } from '../interfaces/user.interface';
import UserService from '../services/user.service';
import HelperResponse from '../utils/herperResponse';

class UserController {

    async createUser(req: Request, res: Response) {

        const user: UserInput = req.body; 

        const validator = await UserService.checkEmailAndUsername(user.email, user.username);
        if(validator?.find) {
            return HelperResponse.sendResponse(res, StatusCodes.CONFLICT, validator.result)
        }

        UserService.create(user)
            .then(result => {
                if(result.hasOwnProperty('id')) {
                    HelperResponse.sendResponse(res, StatusCodes.CREATED, 'User created successfully', user)
                } 
            })
            .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error))

    }

    async updateUser(req: Request, res: Response) {
        const { id } = req.params;
        const user: UserInput = req.body;
        const user_id = String(req.user);

        if(id != req.user) {
            await UserService.checkPermissions(parseInt(user_id)).then(result => {
                if(!result) {
                    return HelperResponse.sendResponse(res, StatusCodes.FORBIDDEN, "Your user doesn't have permission for this action");
                }
            });
        }

        UserService.update(parseInt(id), user)
            .then(result => {
            
                if(result == 'User not found') {
                    HelperResponse.sendResponse(res, StatusCodes.NOT_FOUND, result);
                } else {
                    HelperResponse.sendResponse(res, StatusCodes.ACCEPTED, 'Successfully changed data', result);
                }
            })
            .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error))
    }

    async getUserById(req: Request, res: Response) {
        const { id } = req.params;

        UserService.getById(parseInt(id))
            .then(result => {
            
                if(result == 'User not found') {
                    HelperResponse.sendResponse(res, StatusCodes.NOT_FOUND, result);
                } else {
                    HelperResponse.sendResponse(res, StatusCodes.OK, 'User found successfully', result);
                }
            })
            .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error))

    }

    async deleteUser(req: Request, res: Response) {
        const { id } = req.params;
        const user_id = String(req.user);

        if(id != req.user) {
            await UserService.checkPermissions(parseInt(user_id)).then(result => {
                if(!result) {
                    return HelperResponse.sendResponse(res, StatusCodes.FORBIDDEN, "Your user doesn't have permission for this action");
                }
            });
        }

        UserService.delete(parseInt(id))
            .then(result => {
                if(result == 'User not found') {
                    HelperResponse.sendResponse(res, StatusCodes.NOT_FOUND, result);
                } else {
                    HelperResponse.sendResponse(res, StatusCodes.OK, 'Successfully deleted user');
                }
            })
            .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error))
    }

}
export default new UserController();