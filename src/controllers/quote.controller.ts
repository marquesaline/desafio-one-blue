import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { QuoteInput } from '../interfaces/quote.interface';

import QuoteService from '../services/quote.service';
import UserService from '../services/user.service';
import HelperResponse from '../utils/herperResponse';

class QuoteController {

    async addQuote(req: Request, res: Response) {
        const quote: QuoteInput = req.body;
        const username = req.params.username

        QuoteService.add(quote, username).then(result => {
            if(result.hasOwnProperty('id')) {
                HelperResponse.sendResponse(res, StatusCodes.CREATED,'Quote add successfully', result);
            } 

            if(typeof(result) == 'string') {
                HelperResponse.sendResponse(res, StatusCodes.BAD_REQUEST, result);
            }
        })
        .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error));
    }

    async updateQuote(req: Request, res: Response) {
        const { id, username } = req.params;
        const quote: QuoteInput = req.body;

        const user_id = String(req.user);
        const userLogged = await UserService.getById(parseInt(user_id));

        if(typeof(userLogged) != 'string') {
            if(userLogged.username != username) {
                await UserService.checkPermissions(undefined, userLogged.username).then(result => {
                    if(!result) {
                        return HelperResponse.sendResponse(res, StatusCodes.FORBIDDEN, "Your user doesn't have permission for this action");
                    }
                });
            }
        }

        QuoteService.update(parseInt(id), quote).then(result => {
        
            if(result == 'Quote not found') {
                HelperResponse.sendResponse(res, StatusCodes.NOT_FOUND, result);
            } else {
                HelperResponse.sendResponse(res, StatusCodes.ACCEPTED, 'Successfully changed quote', result);
            }
        })
        .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error));

    }

    async getQuotesByUser(req: Request, res: Response) {
        const username = req.params.username;

        QuoteService.getAllByUsername(username).then(result => {

            if(typeof(result) == 'string') {
                HelperResponse.sendResponse(res, StatusCodes.BAD_REQUEST, result);
            } else {
                HelperResponse.sendResponse(res, StatusCodes.OK, 'Successfully searched quotes by user', result);
            }
        })
        .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error));

    }

    async getQuoteById(req: Request, res: Response) {
        const { id } = req.params;

        QuoteService.getById(parseInt(id)).then(result => {
            
            if(result == 'Quote not found') {
                HelperResponse.sendResponse(res, StatusCodes.NOT_FOUND, result);
            } else {
                HelperResponse.sendResponse(res, StatusCodes.OK, 'Quote found successfully', result);
            }
        })
        .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error))

    }

    async deleteQuote(req: Request, res: Response) {
        const { id, username } = req.params;

        const user_id = String(req.user);
        const userLogged = await UserService.getById(parseInt(user_id));

        if(typeof(userLogged) != 'string') {
            if(userLogged.username != username) {
                await UserService.checkPermissions(undefined, userLogged.username).then(result => {
                    if(!result) {
                        return HelperResponse.sendResponse(res, StatusCodes.FORBIDDEN, "Your user doesn't have permission for this action");
                    }
                });
            }
        }

        QuoteService.delete(parseInt(id)).then(result => {
            if(result == 'Quote not found') {
                HelperResponse.sendResponse(res, StatusCodes.NOT_FOUND, result);
            } else {
                HelperResponse.sendResponse(res, StatusCodes.OK, 'Successfully deleted quote');
            }
        })
        .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error))

    }

    async getAllQuotes(req: Request, res: Response) {

        QuoteService.getQuotes()
            .then(result => {HelperResponse.sendResponse(res, StatusCodes.OK, 'Successfully searched all quotes', result)})
            .catch(error => HelperResponse.sendResponse(res, StatusCodes.INTERNAL_SERVER_ERROR, error));

    }
}

export default new QuoteController();