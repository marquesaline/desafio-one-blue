import bodyParser from 'body-parser';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import express from 'express';
import session from 'express-session';
import methodOverride from 'method-override';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import swaggerUi from 'swagger-ui-express';

import { authUser } from './middlewares/authuser';
import userRoutes from './routes/user.routes';
import quoteRoutes from './routes/quote.routes';
import db_connection from './database/dbConnect';

// const PORT = process.env.PORT || 3000;

class App {
    public app: express.Application;

    constructor () {
        this.app = express();

        db_connection.sync();
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        this.app.use(cors({
            allowedHeaders: [
                "Origin",
                "X-Requested-With",
                "Content-Type",
                "Accept",
                "X-Access-Token",
            ],
            origin: true,
            credentials: true,
            preflightContinue: false,
        }));
        this.app.use(express.json());

        this.app.use(session({
            secret: 'secret',
            resave: false,
            saveUninitialized: true,
        }));
        this.app.use(passport.initialize());
        this.app.use(passport.session());
        passport.use(new LocalStrategy(authUser));
        passport.serializeUser((userObj, done) => {
            done(null, userObj);
        });
        passport.deserializeUser((userObj: any, done) => {
            done(null, userObj);
        });
        
        this.app.use(methodOverride('_method'));
        this.app.use((req, res, next) => {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
            res.setHeader("Access-Control-Allow-Headers", "content-type");
            res.setHeader("Content-Type", "application/json");
            res.setHeader("Access-Control-Allow-Credentials", 'true');
            next();
        });

        this.app.use('', userRoutes);
        this.app.use('', quoteRoutes);
        
        //this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))
    }

}

export default new App();