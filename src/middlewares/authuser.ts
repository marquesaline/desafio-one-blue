import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';

import UserService from '../services/user.service';
import HelperResponse from '../utils/herperResponse';

export async function authUser(username: string, password: string, done: any)  {

    const user = await UserService.getUserByUsername(username);

    if(user == 'User not found') {
        return done (null, false);
    } 

    if(typeof(user) != 'string') {

        if(user.password === password) {
            return done (null, user.id);
        } else {
            return done (null, false)
        }
    }
    
}

export async function checkLoggedin(req: Request, res: Response, next: NextFunction) {
    if(req.isAuthenticated()) {
        return next();
    }

    return res.send({'message': 'You must login with a valid username'});
}