import { Response } from 'express';

class HelperResponse {
    sendResponse = (res: Response, statusCode: number, message?: string, data?: any) => {
        res
            .status(statusCode)
            .json({
                message: message,
                result: data
            });
    }

    
}

export default new HelperResponse();