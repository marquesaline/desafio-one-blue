import { Sequelize } from  'sequelize-typescript';

import { UserModel } from './models/user.model';
import { QuoteModel } from './models/quotes.model';

const db_connection = new Sequelize({
    dialect: 'mysql',
    host: '127.0.0.1',
    username: 'admin',
    password: '123456',
    database: 'blog_db',
    logging: false,
    models: [UserModel, QuoteModel]
});

export default db_connection;