import { Table, Model, Column, DataType } from 'sequelize-typescript';

@Table({
    timestamps: true,
    tableName: 'Users',
})
export class UserModel extends Model {
    
    @Column({
        type: DataType.INTEGER,
        allowNull: false, 
        autoIncrement:true,
        primaryKey: true
    })
    id!: number;

    @Column({
        type: DataType.STRING,
        allowNull: false, 
        unique: true,
    })
    username!: string;

    @Column({
        type: DataType.STRING,
        allowNull: false, 
        unique: true
    })
    email!: string;

    @Column({
        type: DataType.STRING,
        allowNull: false, 
    })
    password!: string;

    @Column({
        type: DataType.BOOLEAN,
        allowNull: false, 
        defaultValue: false
    })
    admin!: false;

}