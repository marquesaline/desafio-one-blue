import { Table, Model, Column, DataType, ForeignKey } from 'sequelize-typescript';
import moment from 'moment';

import { UserModel } from './user.model';

@Table({
    timestamps: true,
    tableName: 'Quotes',
})
export class QuoteModel extends Model {
    
    @Column({
        type: DataType.INTEGER,
        allowNull: false, 
        autoIncrement:true,
        primaryKey: true
    })
    id!: number;

    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.INTEGER,
        allowNull: false, 
    })
    id_user!: number;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    text!: number;

    @Column({
        type: DataType.STRING,
        allowNull: false
    })
    author!: string;

    @Column({
        type: DataType.STRING,
        defaultValue: moment().format('DD-MM-YYYY HH:mm')
    })
    created!: string;

}