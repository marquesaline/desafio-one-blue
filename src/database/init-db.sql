
--
-- Estrutura da tabela `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(100) NOT NULL UNIQUE,
  `email` varchar(80) NOT NULL UNIQUE,
  `password` varchar(30) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Estrutura da tabela `Quotes`
--

CREATE TABLE `Quotes` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `author` varchar(100) NOT NULL,
  `created` varchar(50) NOT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedAt` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `Quotes` ADD CONSTRAINT `user_fk` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `Users` (`id`, `username`, `email`, `password`, `admin`, `createdAt`, `updatedAt`) VALUES
(1, 'admin', 'admin@example.com', '123456', 1, NULL, NULL),
(2, 'Maria', 'maria@email.com', '123456', 0, '2023-02-07', '2023-02-07');

INSERT INTO `Quotes` (`id`, `id_user`, `text`, `author`, `created`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'O importante não é vencer todos os dias, mas lutar sempre.', 'Waldemar Valle Martins', '07-02-2023 20:35', '2023-02-07', '2023-02-07'),
(2, 1, 'Maior que a tristeza de não haver vencido é a vergonha de não ter lutado!', 'Rui Barbosa', '07-02-2023 21:14', '2023-02-08', '2023-02-08'),
(3, 2, 'O importante não é vencer todos os dias, mas lutar sempre.', 'Waldemar Valle Martins', '07-02-2023 21:29', '2023-02-07', '2023-02-07');